﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace LD33
{
    public abstract class Health : MonoBehaviour
    {
        public int MaxHealth = 100;
        public int MinHealth = 0;

        private int _health = 0;
        public int CurrentHealth
        {
            get { return this._health; }
            private set
            {
                this._health = value;
                this._health = Mathf.Clamp(this._health, this.MinHealth, this.MaxHealth);
                if (this._health <= 0)
                {
                    this.Die();
                }
                this.UpdateUI();
            }
        }

        public void Awake()
        {
            this.CurrentHealth = this.MaxHealth;
        }

        public void TakeDamage(int amount)
        {
            this.CurrentHealth -= amount;
        }

        public void Heal(int amount)
        {
            this.CurrentHealth += amount;
        }

        protected abstract void UpdateUI();
        protected abstract void Die();
    }
}