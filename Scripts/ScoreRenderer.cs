﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace LD33
{
    public class ScoreRenderer : MonoBehaviour
    {
        public ScoreManager ScoreManager;
        public List<RectTransform> ScoreTexts;

        public void Awake()
        {
            this.ScoreManager = this.GetComponent<ScoreManager>();
        }

        public void Start()
        {
            var scores = this.ScoreManager.HighScores();
            for (int i = 0; i < scores.Count; i++)
            {
                ((RectTransform)this.ScoreTexts[i].FindChild("Name")).gameObject.GetComponent<Text>().text = scores[i].Name;
                ((RectTransform)this.ScoreTexts[i].FindChild("Score")).gameObject.GetComponent<Text>().text = this.ScoreFormat(scores[i].Score);
            }
        }

        private string ScoreLine(int rank, string name, int score)
        {
            return "";
        }

        private string ScoreFormat(int score)
        {
            return string.Format("{0:n0}", score);
        }
    }
}