﻿using UnityEngine;
using System.Collections;

namespace LD33.AI
{
    public class Bullet : MonoBehaviour
    {
        public void OnCollisionEnter2D(Collision2D c)
        {
            /// TODO - Damage etc.
            
            if (c.gameObject.name == "Character")
                GameObject.Destroy(this.gameObject);
        }
    }
}