﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LD33.Player;
using UnityEngine.UI;


namespace LD33.AI
{
    [RequireComponent(typeof(Transform))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class Human : MonoBehaviour
    {
        [Header("Walking")]
        public float WaitMultiplier = 5f;
        public float WalkSpeed = 1f;
        public float RunSpeed = 2.5f;
        public float MaxRandomDistance = 5f;
        public float ChaseCloseLimit = 1f;

        [Header("Human Objects")]
        public List<GameObject> HumanSprites;
        public GameObject HumanBlood;

        [Header("Attacking")]
        public bool Attacking = false;
        public GameObject BulletPrefab;
        public float BulletSpeed = 5f;
        public float AttackDelay = 0.2f;

        public int Score = 5;
        public Text ScoreText;

        private Transform _transform;
        private Rigidbody2D _rigidbody;
        private GameObject _target;

        public bool Scared { get; private set; }

        public void Awake()
        {
            this.Scared = false;
            this._transform = this.GetComponent<Transform>();
            this._rigidbody = this.GetComponent<Rigidbody2D>();
        }

        public void Start()
        {
            this.SelectSprite();
            this.StartCoroutine("DoWander");
        }

        public void OnCollisionEnter2D(Collision2D c)
        {
            if (c.gameObject.GetComponent<Transform>().position.y > this._transform.position.y + 0.15f)
            {
                if (c.gameObject.name == "Character")
                {
                    c.gameObject.GetComponent<PlayerHealth>().Heal(6);
                    c.gameObject.GetComponent<PlayerController>().Score += this.Score;
                    this.Die();
                }
            }
        }

        public void Attack(GameObject target)
        {
            this._target = target;
            this.Attacking = true;
            this.StopCoroutine("DoWander");
            this.StartCoroutine("DoAttackMove");
            this.StartCoroutine("DoAttackAction");
        }

        private IEnumerator DoAttackMove()
        {
            var targetTransform = this._target.GetComponent<Transform>();
            while (this.gameObject.activeInHierarchy && this._target != null)
            {
                var distance = Mathf.Abs(this._transform.position.x - targetTransform.position.x);
                if (distance >= Random.value * this.ChaseCloseLimit + this.ChaseCloseLimit)
                {
                    var right = this._transform.position.x < targetTransform.position.x;
                    this._rigidbody.velocity = (right ? Vector2.right : Vector2.left) * (Random.value * this.RunSpeed);
                }
                else
                {
                    this._rigidbody.velocity = Vector2.zero;
                }
                yield return null;
            }
            this._rigidbody.velocity = Vector2.zero;
        }

        private IEnumerator DoAttackAction()
        {
            var targetTransform = this._target.GetComponent<Transform>();
            while (this.gameObject.activeInHierarchy && this._target != null)
            {
                var offset = (targetTransform.position - this._transform.position).normalized;

                var bullet = (GameObject)GameObject.Instantiate(this.BulletPrefab, this._transform.position + offset/10f, Quaternion.identity);
                GameObject.Destroy(bullet, 1f);
                //this.GetComponent<AudioSource>().PlayOneShot(this.GetComponent<AudioSource>().clip, 1f);

                var bForce = offset * this.BulletSpeed;
                bullet.GetComponent<Rigidbody2D>().AddForce(bForce, ForceMode2D.Impulse);
                yield return new WaitForSeconds(Random.value + this.AttackDelay);
            }
        }

        private void SelectSprite()
        {
            var index = Random.Range(0, this.HumanSprites.Count - 1);
            var g = (GameObject) GameObject.Instantiate(this.HumanSprites[index]);
            var gt = g.GetComponent<Transform>();
            gt.parent = this._transform;
            gt.localPosition = Vector3.zero;
        }

        private IEnumerator DoWander()
        {
            while (this.gameObject.activeInHierarchy && !this.Scared)
            {
                var direction = Random.value - 0.5f;

                if (this._transform.position.x < -9f)
                {
                    direction = 1;
                }
                if (this._transform.position.x > 97.89f)
                {
                    direction = -1;
                }

                this._rigidbody.velocity = new Vector3(
                    direction * this.WalkSpeed,
                    this._rigidbody.velocity.y,
                    0f);
                yield return new WaitForSeconds(Random.value * this.WaitMultiplier);
            }
            this._rigidbody.velocity = Vector2.zero;
        }

        private void Die()
        {
            this.StopAllCoroutines();
            this.GetComponent<BoxCollider2D>().enabled = false;
            GameObject blood = (GameObject)GameObject.Instantiate(this.HumanBlood, this._transform.position, Quaternion.identity);
            GameObject.Destroy(blood, 1.5f);
            GameObject.Destroy(this.gameObject);
        }
    }
}