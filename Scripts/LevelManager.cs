﻿using System;
using UnityEngine;
using System.Collections;
using LD33.Player;
using UnityEngine.UI;

namespace LD33
{
    public class LevelManager : MonoBehaviour
    {
        public GameObject CanvasInGame;
        public GameObject CanvasEndGame;
        public Transform Buildings;
        public Transform Humans;

        public void Start()
        {
            this.StartCoroutine("CheckGameState");
        }

        public IEnumerator CheckGameState()
        {
            while (this.Buildings != null && this.Humans != null && !this.CanvasEndGame.activeInHierarchy)
            {
                if (this.Buildings.childCount == 0 && this.HumanCount() == 0)
                {
                    this.EndGame();
                    yield return 0;
                }
                yield return new WaitForSeconds(4f);
            }
            yield return 0;
        }

        public int HumanCount()
        {
            return this.Humans.childCount;
        }

        public void EndGame()
        {
            this.CanvasInGame.SetActive(false);
            this.CanvasEndGame.SetActive(true);
            PlayerController pc = ((GameObject) GameObject.Find("Character")).GetComponent<PlayerController>();
            this.CanvasEndGame.transform.FindChild("TextScore").GetComponent<Text>().text =
                String.Format("Score > {0:n0}", pc.Score);
        }

        public void ShowScores()
        {
            Time.timeScale = 1f;
            Application.LoadLevel("Scores");
        }

        public void Play()
        {
            Time.timeScale = 1f;
            Application.LoadLevel("Game");
        }

        public void Retry()
        {
            Time.timeScale = 1f;
            Application.LoadLevel(Application.loadedLevelName);
        }

        public void Quit()
        {
            Time.timeScale = 1f;
            Application.LoadLevel("Menu");
        }

        public void QuitApplication()
        {
            Application.Quit();
        }
    }
}