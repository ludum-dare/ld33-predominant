﻿using UnityEngine;
using System.Collections;
using LD33.Player;

namespace LD33.Camera
{
    public class CameraShake : MonoBehaviour
    {
        public PlayerController Player;
        public float ShakeDuration = 1f;
        public bool ShouldShake = false;

        private float _shakeStartTime = 0f;
        private bool _lastPlayerState = false;

        public void Update()
        {
            if (this._lastPlayerState == true && !this.Player.Jumping)
                this.StartCoroutine(this.DoShakeCamera());

            if (Input.GetKeyDown(KeyCode.L) || this.ShouldShake)
                this.StartCoroutine(this.DoShakeCamera());

            this.ShouldShake = false;
            this._lastPlayerState = this.Player.Jumping;
        }

        private IEnumerator DoShakeCamera()
        {
            this._shakeStartTime = Time.timeSinceLevelLoad;
            while (Time.timeSinceLevelLoad < this._shakeStartTime + this.ShakeDuration)
            {
                this.RandomCameraMove();
                yield return new WaitForSeconds(0.05f);
            }
        }

        private void RandomCameraMove()
        {
            Transform cam = UnityEngine.Camera.main.GetComponent<Transform>();
            Vector3 offset = new Vector3((Random.value - 1f)/10f, (Random.value - 1f)/10f, 0f);
            int dir = Random.value > 0.5 ? 1 : -1;
            cam.position = cam.position + offset * dir;
        }
    }
}