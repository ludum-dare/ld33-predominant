﻿using UnityEngine;
using System.Collections;

namespace LD33.Environment
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Clouds : MonoBehaviour
    {
        public float Speed = 1f;

        private Rigidbody2D _rigidbody;

        public void Awake()
        {
            this._rigidbody = this.GetComponent<Rigidbody2D>();
        }

        public void Start()
        {
            this.Speed = Random.value * this.Speed;
            this._rigidbody.velocity = Vector2.left * this.Speed;
            //this._rigidbody.AddForce();
        }
    }
}