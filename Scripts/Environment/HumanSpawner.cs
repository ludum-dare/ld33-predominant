﻿using UnityEngine;
using System.Collections;

namespace LD33.Environment
{
    public class HumanSpawner : MonoBehaviour
    {
        public GameObject HumanPrefab;
        public Transform HumanParent;
        public float SpawnDelay = 1f;

        private Transform _transform;

        public void Awake()
        {
            this._transform = this.GetComponent<Transform>();
        }

        public void Start()
        {
            this.StartCoroutine("Spawn");
        }

        private IEnumerator Spawn()
        {
            while (this.gameObject.activeInHierarchy)
            {
                GameObject human = (GameObject)GameObject.Instantiate(this.HumanPrefab, this._transform.position, Quaternion.identity);
                human.GetComponent<Transform>().parent = this.HumanParent;
                yield return new WaitForSeconds(this.SpawnDelay);
            }
        }
    }
}