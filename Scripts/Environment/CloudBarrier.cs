﻿using UnityEngine;
using System.Collections;

namespace LD33.Environment
{
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Transform))]
    public class CloudBarrier : MonoBehaviour
    {
        public int CloudLayer = 13;
        public Vector3 Offset = new Vector3(20f, 0f, 0f);

        public void OnTriggerEnter2D(Collider2D c)
        {
            if (c.gameObject.layer == this.CloudLayer)
            {
                c.gameObject.GetComponent<Transform>().position = c.gameObject.GetComponent<Transform>().position + this.Offset;
            }
        }
    }
}