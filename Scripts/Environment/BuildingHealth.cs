﻿using UnityEngine;
using System.Collections;
using LD33.Player;

namespace LD33.Environment
{
    public class BuildingHealth : Health
    {
        public TextMesh Text;
        public GameObject Explosion;

        private Transform _transform;

        public void Start()
        {
            this._transform = this.GetComponent<Transform>();
        }

        protected override void UpdateUI()
        {
            this.Text.text = this.CurrentHealth + "";
        }

        protected override void Die()
        {
            GameObject exp = (GameObject)GameObject.Instantiate(this.Explosion, this._transform.position, Quaternion.identity);
            GameObject.Destroy(exp, 3f);
            GameObject.Destroy(this.gameObject, 0.2f);

            // splash damage?
        }
    }
}