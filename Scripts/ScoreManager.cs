﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LD33.Player;

namespace LD33
{
    [Serializable]
    public class ScoreItem
    {
        public int Score;
        public string Name;
    }

    public class ScoreManager : MonoBehaviour
    {
        public PlayerController Player;
        public List<ScoreItem> Scores;

        private string _currentName = "";
        private int _currentScore = 0;

        private bool _ready = false;
        private string _nameKeyFormat = "HS{0}Name";
        private string _scoreKeyFormat = "HS{0}Score";
        private string[] _hsDefaults = new string[]
        {
                "AAA", "BBB", "CCC", "DDD", "EEE",
                "FFF", "GGG", "HHH", "III", "JJJ"
        };

        public void Awake()
        {
            this.SetupHighScores();
        }

        public void AddScore()
        {
            this.AddScore(this._currentName, this.Player.Score);
        }

        public void AddScore(string name, int score)
        {
            List<ScoreItem> scores = this.HighScores();
            for (int i = 0; i < scores.Count; i++)
            {
                if (score > scores[i].Score)
                {
                    scores.Insert(i, new ScoreItem()
                    {
                        Name = name,
                        Score = score
                    });
                    scores.RemoveAt(scores.Count - 1);
                    break;
                }
            }
            this.SaveHighScores(scores);
        }

        public List<ScoreItem> HighScores()
        {
            while (!this._ready)
            {
                // Do nothing, wait.
            }

            List<ScoreItem> scores = new List<ScoreItem>();
            for (int i = 0; i < 5; i++)
            {
                string nameKey = string.Format(this._nameKeyFormat, i);
                string scoreKey = string.Format(this._scoreKeyFormat, i);

                ScoreItem s = new ScoreItem()
                {
                    Name = PlayerPrefs.GetString(nameKey),
                    Score = PlayerPrefs.GetInt(scoreKey)
                };
                scores.Add(s);
            }

            this.Scores = scores;
            return scores;
        }

        public void SaveHighScores(List<ScoreItem> scores)
        {
            for (int i = 0; i < scores.Count; i++)
            {
                string nameKey = string.Format(this._nameKeyFormat, i);
                string scoreKey = string.Format(this._scoreKeyFormat, i);

                PlayerPrefs.SetString(nameKey, scores[i].Name);
                PlayerPrefs.SetInt(scoreKey, scores[i].Score);
            }
            PlayerPrefs.Save();
            this.Scores = scores;
        }

        public void SetupHighScores()
        {
            for (int i = 0; i < 5; i++)
            {
                string nameKey = string.Format(this._nameKeyFormat, i);
                string scoreKey = string.Format(this._scoreKeyFormat, i);
                if (!PlayerPrefs.HasKey(nameKey))
                {
                    PlayerPrefs.SetString(nameKey, this._hsDefaults[i]);
                    PlayerPrefs.SetInt(scoreKey, 10);
                    continue;
                }

                if (!PlayerPrefs.HasKey(scoreKey))
                {
                    PlayerPrefs.SetInt(scoreKey, 10);
                }

            }
            PlayerPrefs.Save();

            this._ready = true;
        }

        public void SetCurrentName(string name)
        {
            this._currentName = name;
        }

        public void SetCurrentScore(int score)
        {
            this._currentScore = score;
        }
    }
}