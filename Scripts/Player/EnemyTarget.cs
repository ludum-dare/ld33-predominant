﻿using UnityEngine;
using System.Collections;
using LD33.AI;

namespace LD33.Player
{
    public class EnemyTarget : MonoBehaviour
    {
        public int HumanLayer = 12;

        public void OnTriggerEnter2D(Collider2D c)
        {
            if (c.gameObject.layer != this.HumanLayer)
                return;

            Human human = c.gameObject.GetComponent<Human>();

            if (!human.Attacking)
                human.Attack(this.gameObject);
        }
    }
}