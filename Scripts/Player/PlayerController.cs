﻿using System;
using UnityEngine;
using System.Collections;
using LD33.Environment;
using UnityEngine.UI;

namespace LD33.Player
{
    [RequireComponent(typeof(Transform))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour
    {
        private Transform _transform;
        private Rigidbody2D _rigidbody;

        private Vector2 _targetVelocity = Vector2.zero;
        private bool _facingRight = true;

        private bool _wantJump = false;

        private bool _isJumping = false;
        public bool Jumping
        {
            get { return this._isJumping; }
            private set { this._isJumping = value; }
        }

        [Header("Movement")]
        public bool Controllable = true;
        public float MoveSpeed = 0.5f;

        [Header("Jumping")]
        public float JumpForce = 1f;
        public float GroundDistance = 0.1f;
        public ForceMode2D JumpForceMode = ForceMode2D.Impulse;

        [Header("Character Modes")]
        public GameObject GoodModeObject;
        public GameObject BadModeObject;
        public int ModeFadeStep = 2;

        [Header("Damage and Attack")]
        public int GoodModeDamage = 10;
        public int BadModeDamage = 25;
        public Text ScoreText;
        public int BulletDamage = 2;

        private int _score = 0;
        public int Score
        {
            get { return this._score; }
            set
            {
                this._score = value;
                this.UpdateScoreUI();
            }
        }

        private bool _isGoodMode = true;
        public bool GoodMode
        {
            get { return this._isGoodMode; }
            private set { this._isGoodMode = value; }
        }
        private bool _isModeSwapping = false;

        public void Awake()
        {
            this._transform = this.GetComponent<Transform>();
            this._rigidbody = this.GetComponent<Rigidbody2D>();
        }

        public void Update()
        {
            if (this.Controllable)
            {
                this._targetVelocity += this.MovementControls();
                this.CharacterDirection();

                if (!this.Jumping && Input.GetKeyDown(KeyCode.Space))
                    this._wantJump = true;

                if (Input.GetKeyDown(KeyCode.P) && !this._isModeSwapping)
                {
                    this.StartCoroutine(this.SwapMode());
                }
            }
        }

        public void FixedUpdate()
        {
            if (this.ShouldJump())
            {
                this._rigidbody.AddForce(Vector2.up * this.JumpForce, this.JumpForceMode);
                this.Jumping = true;
                this._wantJump = false;
            }

            var velocity = new Vector2(
                this._targetVelocity.normalized.x * this.MoveSpeed,
                this._rigidbody.velocity.y);
            this._rigidbody.velocity = velocity;
        }

        public void LateUpdate()
        {
            if (this.Jumping)
                this.CheckGround();
        }

        public void OnCollisionEnter2D(Collision2D c)
        {
            // Ammo
            if (c.gameObject.layer == 14)
            {
                this.gameObject.GetComponent<PlayerHealth>().TakeDamage(this.BulletDamage);
                GameObject.Destroy(c.gameObject);
            }

        }

        private Vector2 MovementControls()
        {
            var movement = Vector2.zero;
            if (Input.GetKeyDown(KeyCode.D))
                movement += Vector2.right;
            if (Input.GetKeyUp(KeyCode.D))
                movement -= Vector2.right;
            if (Input.GetKeyDown(KeyCode.A))
                movement += Vector2.left;
            if (Input.GetKeyUp(KeyCode.A))
                movement -= Vector2.left;

            return movement;
        }

        private void CharacterDirection()
        {
            if (this._targetVelocity.x > 0 && !this._facingRight)
                this.Flip();
            if (this._targetVelocity.x < 0 && this._facingRight)
                this.Flip();
        }

        private bool ShouldJump()
        {
            return this._wantJump && !this.Jumping;
        }

        private void CheckGround()
        {
            //Debug.DrawRay(this._transform.position, Vector3.down, Color.red, 3f);
            Debug.DrawLine(this._transform.position, this._transform.position + Vector3.down * this.GroundDistance, Color.red, 3f);

            var mask = 1 << LayerMask.NameToLayer("Ground");
            mask |= 1 << LayerMask.NameToLayer("Human");
            mask |= 1 << LayerMask.NameToLayer("Building");

            Collider2D col = null;

            for (int i = -5; i <= 5; i++)
            {
                var x = i / 5f * 0.3f;
                var hit = Physics2D.Raycast(
                    this._transform.position + new Vector3(x, 0f, 0f),
                    Vector2.down,
                    this.GroundDistance,
                    mask);

                if (hit.collider != null)
                {
                    col = hit.collider;
                    break;
                }
            }

            if (col != null)
            {
                this.Jumping = false;

                if (col.gameObject.tag == "Building")
                {
                    var amount = this.GoodMode ? this.GoodModeDamage : this.BadModeDamage;
                    this.Score += amount;
                    col.gameObject.GetComponent<BuildingHealth>().TakeDamage(amount);
                }
            }
        }

        private void Flip()
        {
            this._facingRight = !this._facingRight;
            var newScale = this._transform.localScale;
            newScale.x *= -1;
            this._transform.localScale = newScale;
        }

        private IEnumerator SwapMode()
        {
            this._isModeSwapping = true;
            var goodRenderer = this.GoodModeObject.GetComponent<SpriteRenderer>();
            var badRenderer = this.BadModeObject.GetComponent<SpriteRenderer>();

            var fadeIn = this._isGoodMode ? badRenderer : goodRenderer;
            var fadeOut = this._isGoodMode ? goodRenderer : badRenderer;
            var fadeInColor = fadeIn.color;
            var fadeOutColor = fadeOut.color;

            for (var i = 0; i <= 255; i += this.ModeFadeStep)
            {
                float f = i/255f;
                fadeInColor.a = f;
                fadeOutColor.a = 1f - f;
                fadeIn.color = fadeInColor;
                fadeOut.color = fadeOutColor;
                yield return null;
            }
            this._isGoodMode = !this._isGoodMode;
            this._isModeSwapping = false;
        }

        private void UpdateScoreUI()
        {
            this.ScoreText.text = String.Format("Score > {0:n0}", this.Score);
        }
    }
}