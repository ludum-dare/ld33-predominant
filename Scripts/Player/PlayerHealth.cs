﻿using UnityEngine;
using System.Collections;
using LD33;
using UnityEngine.UI;
using System;
using LD33.Camera;

namespace LD33.Player
{
    public class PlayerHealth : Health
    {
        public Slider Slider;
        public LevelManager LevelManager;

        private bool _dead = false;

        protected override void UpdateUI()
        {
            this.Slider.value = Mathf.Clamp(this.CurrentHealth, this.MinHealth, this.MaxHealth);
        }

        protected override void Die()
        {
            if (this._dead)
                return;

            this._dead = true;
            this.StartCoroutine("PlayerDeath");
            this.LevelManager.EndGame();
        }

        private IEnumerator PlayerDeath()
        {
            this.GetComponent<PlayerController>().Controllable = false;
            CameraShake cameraShake = UnityEngine.Camera.main.GetComponent<CameraShake>();
            cameraShake.ShakeDuration = 5f;
            cameraShake.ShouldShake = true;
            yield return new WaitForSeconds(cameraShake.ShakeDuration);

            Time.timeScale = 0f;
        }
    }
}